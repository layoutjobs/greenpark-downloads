<?php 

$files = [
	[
		'image' => 'anuncios-revistas-jornais.jpg',
		'title' => 'Anúncios para revistas e jornais',
		'description' => 'Anúncios para serem veiculados em revistas e jornais. Formato: PDF.',
		'file' => 'anuncios-revistas-jornais.pdf',
	],
	[
		'image' => 'apresentacao-powerpoint.jpg',
		'title' => 'Apresentação em PowerPoint',
		'description' => 'Apresentação em PowerPoint sobre o empreendimento. Acompanha vídeo em formato PPS e MP4.',
		'file' => 'apresentacao-powerpoint.zip',
	],
	[
		'image' => 'apresentacao-email.jpg',
		'title' => 'Apresentação para e-mail',
		'description' => 'Arte de divulgação do empreendimento para ser enviada por e-mail. Formato: PDF.',
		'file' => 'apresentacao-email.pdf',
	],
	[
		'image' => 'banner-corretores.jpg',
		'title' => 'Banner para corretores',
		'description' => 'Arte de banner para uso dos corretores, com 1,93m de largura x 2,5m de altura. Formato: PDF.',
		'file' => 'banner-corretores.pdf',
	],
	[
		'image' => 'folder.jpg',
		'title' => 'Folder',
		'description' => 'Arte de folder para impressão. Formato: PDF. Formato aberto: 560mm de largura x 198mm de altura. Formato fechado: 280mm de largura x 198mm de altura.',
		'file' => 'folder.pdf',
	],
	[
		'image' => 'folheto.jpg',
		'title' => 'Folheto',
		'description' => 'Arte de folheto para impressão, com 100mm de largura x 210mm de altura. Formato: PDF.',
		'file' => 'folheto.pdf',
	],
	[
		'image' => 'fotos-3d.jpg',
		'title' => 'Fotos em 3D',
		'description' => 'Diversas imagens em 3D do exterior e do interior do empreendimento. Formato: JPG.',
		'file' => 'fotos-3d.zip',
	],
	[
		'image' => 'fotos-drone.jpg',
		'title' => 'Fotos de drone',
		'description' => 'Fotografias tiradas por drone sobrevoando os arredores do empreendimento, mostrando sua vista privilegiada. Formato: JPG.',
		'file' => 'fotos-drone.zip',
	],
	[
		'image' => 'fotos-obra.jpg',
		'title' => 'Fotos da obra',
		'description' => 'Fotografias mostrando o andamento da obra. Atualizado em: 23/11/2015. Formato: JPG.',
		'file' => 'fotos-obra.zip',
	],
	[
		'image' => 'logotipo.jpg',
		'title' => 'Logotipo',
		'description' => 'Logotipo do empreendimento. Formato: PDF.',
		'file' => 'logotipo.pdf',
	],
	[
		'image' => 'mailings.jpg',
		'title' => 'Mailings',
		'description' => 'Quatro artes diferentes para mailings. Formato: JPG.',
		'file' => 'mailings.zip',
	],
	[
		'image' => 'posts-facebook.jpg',
		'title' => 'Posts para Facebook',
		'description' => 'Seis artes de posts diferentes para serem utilizadas no Facebook. Formato: JPG.',
		'file' => 'posts-facebook.zip',
	],
	[
		'image' => 'manual-corretor.jpg',
		'title' => 'Manual do corretor',
		'description' => 'Manual do corretor com todas as informações necessárias sobre o empreendimento. Formato: PDF.',
		'file' => 'manual-corretor.pdf',
	],
	[
		'image' => 'memorial.jpg',
		'title' => 'Memorial descritivo',
		'description' => 'Memorial descritivo do empreendimento. Acompanha Minuta no mesmo arquivo. Formato: PDF.',
		'file' => 'memorial.pdf',
	],
	[
		'image' => 'video.jpg',
		'title' => 'Vídeo',
		'description' => 'Vídeo de apresentação do empreendimento, mostrando sua infraestrutura, localização e outros dados importantes. Formato: MP4.',
		'file' => 'video.mp4',
	],
	[
		'image' => 'memorial-acabamentos.jpg',
		'title' => 'Memorial descritivo de acabamentos',
		'description' => 'Memorial descritivo de acabamentos. Formato: PDF.',
		'file' => 'memorial-acabamentos.pdf',
	],
];
?>

<div class="file-list">

	<?php foreach ($files as $file) : ?>

	<div class="item">
		<div class="col">
			<figure class="file-image">
				<img src="files/images/<?= $file['image'] ?>">
			</figure>
		</div>
		<div class="col">
			<h4 class="heading-primary"><b><?= $file['title'] ?></b></h4>
			<p><?= $file['description'] ?></p>
		</div>
		<div class="col"><a class="btn btn-block btn-lg btn-primary" href="files/<?= $file['file'] ?>" target="_blank">Baixar Arquivo</a></div>
	</div>

	<?php endforeach; ?>

</div>