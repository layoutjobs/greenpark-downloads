<?php

$login = [
	'username' => '', 
	'password' => '',
];

if (isset($_POST['Login'])) {
	$login = array_merge($login, $_POST['Login']);
	App::login($login);
}
?>

<div class="row">
	<div class="col-md-6 col-md-offset-3 text-center">
		<h3 class="heading heading-primary">Login</h3>
		<form action="" method="post">
			<div class="form-group">
				<label class="form-control-label sr-only" for="login-username">Nome de usuário</label>
				<input class="form-control" id="login-username" type="text" name="Login[username]" value="<?= $login['username'] ?>" placeholder="Nome de usuário">
			</div>
			<div class="form-group">
				<label class="form-control-label sr-only" for="login-username">Senha</label>
				<input class="form-control" id="login-password" type="password" name="Login[password]" value="<?= $login['password'] ?>" placeholder="Senha">
			</div>
			<button class="btn btn-block btn-primary" type="submit">Entrar</button>
		</form>
	</div>
</div>

