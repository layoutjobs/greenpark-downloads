<?php

App::start();

class App
{
	const SESSION_KEY = 'Yud232das@#dhUYExZXX3cXsd45X#x';

	public static $config = [];
	public static $errors = [];

	public static function start()
	{
		static::$config = require(__DIR__ . DIRECTORY_SEPARATOR . 'config.php');

		$page = ($page = Request::getRoute()) ? $page : 'home';

		if (View::exists('app/pages/' . $page)) {
			$content = View::render('app/pages/' . $page);
		} else {
			header('HTTP/1.0 404');
			$content = View::render('app/pages/404');
		}

		session_start();

		if (isset($_SESSION[static::SESSION_KEY])) {
			$session = $_SESSION[static::SESSION_KEY];
		} elseif ($page !== 'login' && $page !== '404') {
			Request::redirect('login');
		}

		echo View::render('app/layout', ['content' => $content]);
	}

	public static function login($data)
	{
		if (empty($data['username']) || empty($data['password'])) {
			static::$errors[] = 'Usuário ou senha não informados.'; 
			return false;
		} 

		if ($data['username'] == static::$config['login.username'] && $data['password'] == static::$config['login.password']) {
			session_start();
			$_SESSION[static::SESSION_KEY] = $data;
			Request::redirect();
		} else {
			static::$errors[] = 'Usuário ou senha inválidos.'; 
		}
	}

	public static function logout()
	{
		session_start();
		unset($_SESSION[static::SESSION_KEY]);
		Request::redirect('login');
	}
}

class Request
{
	public static function getBasePath($absolute = true)
	{
		$root = trim(realpath($_SERVER['DOCUMENT_ROOT']), DIRECTORY_SEPARATOR);
		$current = trim(realpath(dirname($_SERVER['SCRIPT_FILENAME'])), DIRECTORY_SEPARATOR);
		$basePath = str_replace(DIRECTORY_SEPARATOR, '/', trim(str_replace($root, '', $current), DIRECTORY_SEPARATOR)); 
		return ($absolute ? $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR : '') . $basePath;
	}

	public static function getBaseUrl()
	{
		return '/' . static::getBasePath(false) . '/';
	}

	public static function getRoute()
	{
		if (isset($_GET['r'])) {
			return $_GET['r'];
		} else {
			return str_replace(static::getBaseUrl(), '', $_SERVER['REQUEST_URI']);
		}
	}

	public static function redirect($to = null)
	{
		header('Location: ' . static::getBaseUrl() . $to);
	}
}

class View
{
	public static function exists($path)
	{
		return file_exists(static::normalizePath($path));
	}

	public static function render($path, array $params = [])
	{
		if ($params != null) {
			foreach ($params as $name => $value) {
				global $$name;
				$$name = $value;
			}
		}
		ob_start();
		require(static::normalizePath($path));
		$var = uniqid();
		$$var = ob_get_contents();
		ob_end_clean();
		foreach ($params as $name => $value) {
			unset($$name);
		}
		return $$var;
	}

	protected static function normalizePath($path)
	{
		$path = trim(str_replace('/', DIRECTORY_SEPARATOR, $path), DIRECTORY_SEPARATOR);
		return Request::getBasePath(true) . DIRECTORY_SEPARATOR . $path . '.php';
	}
}
