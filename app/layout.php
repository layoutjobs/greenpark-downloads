<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>Green Park - Área de Downloads</title>
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/app.min.css" rel="stylesheet">
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<link href="//fonts.googleapis.com/css?family=Lato:300,700" rel="stylesheet">
	</head>
	<body>
		<header class="page-header">
			<div class="container">
				<div class="page-header-logo-teamprime">
					<img src="img/logo-teamprime.png" alt="Team Prime">
				</div>
				<h1 class="page-header-heading">
					<span><span>Área de</span> <span>Downloads</span></span>
				</h1>
			</div>
		</header>
		<aside class="page-intro">
			<div class="container">
				<div class="page-intro-logo"><img src="img/logo-greenpark-moldura.png" alt="Green Park"></div>
			</div>
		</aside>
		<main class="page-body">
			<div class="container">
				
				<?php foreach (App::$errors as $error) : ?>

				<p class="alert alert-danger"><b>Ops!</b> <?= $error ?></p>

				<?php endforeach; ?>

				<?php echo $content; ?>
			</div>		
		</main>
		<footer class="page-footer">
			<div class="container">
				Copyright &copy; Green Park. Todos os direitos reservados. 
			</div>
		</footer>
		<script src="vendor/bower/jquery/dist/jquery.min.js"></script>
		<script src="vendor/bower/bootstrap/dist/js/bootstrap.min.js"></script>
	</body>
</html>